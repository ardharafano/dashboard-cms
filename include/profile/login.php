<div id="login">
    <div class="container">
        <div class="authentication-wrapper authentication-basic container-p-y">
            <div class="authentication-inner">
                <!-- Register -->
                <div class="card">
                    <div class="card-body">
                        <!-- Logo -->
                        <div class="app-brand justify-content-center">
                            <a href="?profile=login" class="app-brand-link gap-2">
                                <span class="app-brand-logo demo">
                                <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47" />
                              </span>
                            </a>
                        </div>
                        <!-- /Logo -->
                        <h4>Hello,</h4>
                        <h2>Welcome!</h2>

                        <form id="formAuthentication" class="mb-3" action="?profile=home" method="POST">

                            <div class="mb-3 text-center">
                                <label for="partner" class="form-label">Partner Name</label>
                                <select class="form-select" aria-label="Default select example">
                            <option selected>Pilih Partner</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                            </div>

                            <div class="mb-3 text-center">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email-username" placeholder="Enter your email" autofocus />
                            </div>
                            <div class="mb-3 form-password-toggle">
                                <div class="text-center">
                                    <label class="form-label" for="password">Password</label>
                                </div>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="remember-me" />
                                    <label class="form-check-label" for="remember-me"> Remember Me </label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button class="btn login d-grid w-100" type="submit">LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /Register -->
            </div>
        </div>
    </div>
</div>

<!-- / Content -->