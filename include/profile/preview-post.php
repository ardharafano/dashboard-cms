<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            <div class="app-brand demo">
                <a href="?profile=home" class="app-brand-link" target="_blank">
                    <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47" class="logo-side" />
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                    <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
            </div>

            <div class="menu-inner-shadow"></div>

            <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="?profile=home" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle"></i>
                        <div data-i18n="Analytics">Dashboard</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text">Konten</span>
                </li>

                <li class="menu-item open">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">Post</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item active">
                            <a href="?profile=all-post" class="menu-link">
                                <!-- <i class='menu-icon bx bxs-message-alt-detail'></i> -->
                                <div data-i18n="Account">All Post</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=new-post" class="menu-link">
                                <!-- <i class='menu-icon bx bx-edit'></i> -->
                                <div data-i18n="Notifications">New</div>
                            </a>
                        </li>
                    </ul>
                </li>



                <li class="menu-item">
                    <a href="?profile=pages-topup" class="menu-link">
                        <i class='menu-icon bx bx-photo-album'></i>
                        <div data-i18n="Analytics">Galeri</div>
                    </a>
                </li>

                <!-- Forms & Tables -->
                <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                        <div data-i18n="Account Settings">Account Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-password" class="menu-link">
                                <div data-i18n="Notifications">Password</div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">

            <div class="card-bg my-3">
                <!-- <h5 class="card-account">Profile Details</h5> -->

                <header>
                    <div class="container">
                        <div class="row">
                            <h1 class="h3 mb-1 ps-5 fw-bold mt-5 text-white">All Galleries</h1>
                        </div>
                    </div>
                </header>

            </div>


            <!-- content wrapper -->
            <div class="content-wrapper">
                <!-- Dashboard  -->
                <div class="container-xxl flex-grow-1 container-p-y">
                    <div class="card mb-4" style="position:relative;top:-150px">
                        <hr class="my-0" />

                        <div id="preview-post">
                            <div class="container">
                                <div class="d-lg-flex justify-content-between align-content-center align-items-center my-4">
                                    <div>
                                        <h1 class="h3 fw-bold mt-3 ps-1" style="color:#440C0C">PREVIEW PAGE</h1>
                                    </div>

                                    <div class="d-flex justify-content-center">
                                        <a href="#" class="menu-link button-edit">
                                            <i class='menu-icon bx bx-edit-alt'></i>
                                            <div>Edit</div>
                                        </a>

                                        <div>
                                        <a href="#" class="menu-link button-unpublish" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                                            <i class='menu-icon bx bx-share'></i>
                                            <div>Unpublish</div>
                                        </a>
                                        </div>

                                    </div>
                                </div>

                                <h3 style="color:#2B2B2B">Hasil Liga Italia: AC Milan Menang 2-1 di Markas Salernitana</h3>

                                <hr class="my-0">
                                <div class="topic">Topic: Liga Italia</div>
                                <div class="author">Author: Ronald S</div>
                                <div class="date">Wednesday, 04 Jan 2023 | 22:18 WIB</div>
                                <hr class="my-0">

                                <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="653" height="367" class="terkait" />
                                <figcaption>Ilustrasi logo Liga Italia Serie A. [ANTARA/Gilang Galiartha]</figcaption>
                                <p>Hasil Liga Italia, AC Milan memetik kemenangan 2-1 atas Salernitana 2-1 di Stadio Arechi pada Rabu (4/1/2023) malam WIB.</p>
                                <p>Rossoneri bahkan sudah unggul dua gol di babak pertama lewat gol Rafael Leao dan Sandro Tonali. Tuan rumah bisa memperkecil ketertinggalan berkat gol Federico Bonazzoli.</p>
                                <p>Kemenangan ini memantapkan AC Milan di posisi kedua dengan 36 poin, tertinggal lima angka dari Napoli di puncak klasemen. Sementara itu, Salernitana ada di urutan ke-13 dengan 17 poin.</p>
                                <p>AC Milan langsung tampil menyerang sejak pertandingan dimulai. Mereka hampir mencetak gol pada menit kelima, andai saja tembakan Rafael Leao tidak diselamatkan kiper Guillermo Ochoa.</p>
                                <p>Leao baru bisa membuka skor pada menit ke-10. Penyerang Portugal itu melepaskan tembakan dari sisi kotak penalti dan bola gagal dibendung Ochao. Milan memimpin 1-0 atas Salernitana.</p>

                                <div class="button-back" onclick="history.back()">Back</div>

                            </div>



                        </div>


                        <div class="content-backdrop fade"></div>
                    </div>
                    <!-- Content wrapper -->
                </div>
                <!-- / Layout page -->
            </div>
            <!-- end content wrapper -->


            <!-- <div class="content-backdrop fade"></div> -->
        </div>
        <!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->