<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

    <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="?profile=home" class="app-brand-link" target="_blank">
            <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47" class="logo-side" />
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item active">
            <a href="?profile=home" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>


        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Konten</span>
        </li>
        
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="Account Settings">Post</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="?profile=all-post" class="menu-link">
                    <!-- <i class='menu-icon bx bxs-message-alt-detail'></i> -->
                        <div data-i18n="Account">All Post</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="?profile=new-post" class="menu-link">
                    <!-- <i class='menu-icon bx bx-edit'></i> -->
                        <div data-i18n="Notifications">New</div>
                    </a>
                </li>
            </ul>
        </li>



        <li class="menu-item">
            <a href="?profile=galeri" class="menu-link">
                <i class='menu-icon bx bx-photo-album'></i>
                <div data-i18n="Analytics">Galeri</div>
            </a>
        </li>

        <!-- Forms & Tables -->
        <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                <div data-i18n="Account Settings">Account Settings</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="?profile=pages-account-settings-password" class="menu-link">
                        <div data-i18n="Notifications">Password</div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</aside>
<!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">

            <div class="card-bg my-3">
                <!-- <h5 class="card-account">Profile Details</h5> -->

                <header>
                    <div class="container">
                        <div class="row">
                            <h1 class="h3 mb-1 ps-5 fw-bold mt-5 text-white">User Account</h1>
                            <p class="ps-5 mb-5 text-white">Experience a simple yet powerful way to build Dasboards with Arkadia</p>
                        </div>
                    </div>
                </header>

                <!-- Account -->
                <div class="card-body">
                    <div class="d-flex align-items-start align-items-sm-center gap-4">
                        <img src="assets/vendor/images/1.png" alt="user-avatar" class="card-bg-img" height="100" width="100" id="uploadedAvatar" />
                    </div>
                    <div class="card-bg-username">iklandisini</div>
                </div>
            </div>

            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Content -->

                <div class="container-xxl flex-grow-1 container-p-y">
                    <!-- <h5 class="card-account">Rekomendasi Artikel</h5>
                    <div class="rekomendasi-artikel">
                        <div class="row">
                            <div class="col-lg-3 col-6">
                                <img src="assets/vendor/images/artikel.png" alt="img" />
                                <a href="#">
                                    <p>Ridwan Kamil Jadi Kepala Daerah Terbaik di Masa Pandemi Covid-19</p>
                                </a>
                            </div>
                            <div class="col-lg-3 col-6">
                                <img src="assets/vendor/images/artikel.png" alt="img" />
                                <a href="#">
                                    <p>Ridwan Kamil Jadi Kepala Daerah Terbaik di Masa Pandemi Covid-19</p>
                                </a>
                            </div>
                            <div class="col-lg-3 col-6">
                                <img src="assets/vendor/images/artikel.png" alt="img" />
                                <a href="#">
                                    <p>Ridwan Kamil Jadi Kepala Daerah Terbaik di Masa Pandemi Covid-19</p>
                                </a>
                            </div>
                            <div class="col-lg-3 col-6">
                                <img src="assets/vendor/images/artikel.png" alt="img" />
                                <a href="#">
                                    <p>Ridwan Kamil Jadi Kepala Daerah Terbaik di Masa Pandemi Covid-19</p>
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-12">

                            <div class="card mt-4">


                                <div class="user-info-wrap">
                                    <!-- <div class="user-info">
                                        <span class="user-info-title">Username</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div> -->

                                    <div class="user-info">
                                        <span class="user-info-title">Nama Lengkap</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Email</span>
                                        <span class="user-info-title">: bram@gmail.com</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Bio</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Alamat</span>
                                        <span class="user-info-title">: Jakarta, jakarta, Indonesia, 17660</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Jenis Kelamin</span>
                                        <span class="user-info-title">: Laki - Laki</span>
                                    </div>
                                    <!-- 
                                    <div class="user-info">
                                        <span class="user-info-title">Telepon</span>
                                        <span class="user-info-title">0812789898</span>
                                    </div> -->

                                    <div class="user-info">
                                        <span class="user-info-title">Tanggal Lahir</span>
                                        <span class="user-info-title">: Jakarta, 01-01-2022</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Pekerjaan</span>
                                        <span class="user-info-title">: Karyawan</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Facebook</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Instagram</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Twitter</span>
                                        <span class="user-info-title">: iklandisini</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Provinsi</span>
                                        <span class="user-info-title">: Jakarta</span>
                                    </div>

                                    <div class="user-info">
                                        <span class="user-info-title">Kota</span>
                                        <span class="user-info-title">: Jakarta</span>
                                    </div>

                                </div>

                            </div>

                            <div class="mt-3 d-flex justify-content-center">
                                <!-- <a href="?profile=pages-account-settings-account">
                                    <button type="submit" class="btn btn-primary me-2">Edit Profile</button>
                                </a> -->
                                <a href="?profile=pages-account-settings-password">
                                    <button type="submit" class="btn btn-primary me-2">Edit Password</button>
                                </a>
                            </div>

                            <!-- /Account -->
                        </div>

                    </div>
                </div>
            </div>

            <!-- / Content -->


            <div class="content-backdrop fade"></div>
        </div>
        <!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
</div>


<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->